# Cloud 9 With React
## About
This project is an expirement to see if we can get React to run in Cloud 9 for the purpose 
of evaluating the feasibility of using this set of tools to build a UI for IPAS

## Resources
1. [React Installation Docs](https://facebook.github.io/react/docs/installation.html)
2. [Web Pack Issues Log (emckay commented on May 11, 2016)](https://github.com/webpack/webpack-dev-server/issues/230)
3. [React Form Example](http://lorenstewart.me/2016/10/31/react-js-forms-controlled-components/)

## Setting Up Your Cloud9 Workspace with Git and Bitbucket the first time.
1. Create an account at [bitbucket.org](https://bitbucket.org/)
    - Click on get started and complete the registration form
    - Ping/email me with your bitbucket user id so I can share the repo with you
2. Create an account at [cloud9.io](https://c9.io/). You will be asked for a credit/debit card, but you will not be charged for the free plan.
3. Link your new bitbucket account to Cloud9
    - Navigate to your account settings. (gear at the top right of your home page)
    - In the left menu select Connected Services
    - Then select Bitbucket.  A popup window should open asking for your bitbucket credentials.
    - Log in.  Your accounts are now linked.
4. Create a new Workspace
    - Click the Create a new workspace tile
    - Name your new workspace mymap-student
    - Paste "git@bitbucket.org:dbrown117/mymap-builder-for-students.git" in for the Clone from Git URL
    - Choose blank for the template
    - Click Create workspace
5. Upgrade Node to version 7.3.0 and make default (bash console at the bottom of the screen)
    ```shell
    $ nvm install 7.3.0
    
    $ nvm alias default 7.3.0
    ```
6. Run npm install (this should create a folder called node_modules)
    ```shell
    $ npm install
    ```
7. Modify the node_modules/react-scripts/config/webpack.config.dev.js file
    ```JavaScript
    55 require.resolve('webpack-dev-server/client') + '?https://0.0.0.0:8080', //uncomment and add replace / with https://0.0.0.0:8080
    
    56 require.resolve('webpack/hot/dev-server'),  //uncomment
    
    57 //require.resolve('react-dev-utils/webpackHotDevClient'), //comment this line
    ```
8. Start the Dev Server
    ```shell
    $ npm run start
    ```
9. Test the Application
    - At the top of the page look for a menu item "Preview" Click then select "Preview running application"
    - In the new panel open in a new tab by clicking the icon with an arrow pointing up and left inside a box.
10. Shut down your dev server by hitting Cntrl + C in the console.