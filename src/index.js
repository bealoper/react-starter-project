import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';

function render(container){               
 
  if(container) {
    ReactDOM.render(
      <App/>,
      container
    );
  }
}

const container = document.getElementById('app');

if(container) {
    
    render(container);

} else {
    alert("fatal error missing container element on page");
}